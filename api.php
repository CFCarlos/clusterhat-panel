<?php

$pi     = htmlspecialchars($_GET["pi"]);
$action = htmlspecialchars($_GET["action"]);
$port   = preg_replace('/[^0-9]+/', '', 608 . $pi);

if ($action == "status") {
echo    shell_exec("bash ./scripts/status $pi");
exit;
}

if (($pi == "master") && ($action == "reboot")) {
    shell_exec("sudo reboot");

} else {
    if ($action == "vnc") {
        $passwd = bin2hex(random_bytes(15));
        $url = preg_replace('/:[0-9]+/', '', ($_SERVER['HTTP_HOST']));
        $vncurl = "http://" . $url . ":" . "6081/";
	shell_exec("ssh  -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa pi@$pi " . '"echo ' . $passwd . '| vncpasswd -f > ~/.vnc/passwd"');
        header('Location: ' . $vncurl . 'vnc.html?host=' . $url . '&port=' . $port . '&password=' . $passwd . '&autoconnect=true');
        exit;
    }

    shell_exec("bash ./scripts/start $action $pi");
    header('Location: index.php');
    exit;
}
?>
