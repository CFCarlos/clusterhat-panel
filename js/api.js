    function upsh() {
        $('#p1').load('/api.php?pi=p1&action=status');
        $('#p2').load('/api.php?pi=p2&action=status');
        $('#p3').load('/api.php?pi=p3&action=status');
        $('#p4').load('/api.php?pi=p4&action=status');
    }
setInterval("upsh()", 1000);


function startp1() {
    $.ajax({
        url: '/api.php?pi=p1&action=on',
        complete: function(response) {},
    });
    return false;
}


function stopp1() {
    $.ajax({
        url: '/api.php?pi=p1&action=off',
        complete: function(response) {},
    });
    return false;
}


function startp2() {
    $.ajax({
        url: '/api.php?pi=p2&action=on',
        complete: function(response) {},
    });
    return false;
}


function stopp2() {
    $.ajax({
        url: '/api.php?pi=p2&action=off',
        complete: function(response) {},
    });
    return false;
}


function startp3() {
    $.ajax({
        url: '/api.php?pi=p3&action=on',
        complete: function(response) {},
    });
    return false;
}


function stopp3() {
    $.ajax({
        url: '/api.php?pi=p3&action=off',
        complete: function(response) {},
    });
    return false;
}


function startp4() {
    $.ajax({
        url: '/api.php?pi=p4&action=on',
        complete: function(response) {},
    });
    return false;
}


function stopp4() {
    $.ajax({
        url: '/api.php?pi=p4&action=off',
        complete: function(response) {},
    });
    return false;
}


function reboot() {
    if (confirm("Are you sure? The controller will reboot instantly!")) {
        $.ajax({
            url: '/api.php?pi=master&action=reboot'
        })
    }
};
