
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/api.js"></script>

	<title>ClusterHat Panel 0.1</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="imIP/png" href="imIPs/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>



	<div class="limiter">

		<div class="container-table100">

			<div class="wrap-table100">
			<center>
			<img src="header.png" width=80%>
			</center>
				<br>
				</br>
					<div class="table">

						<div class="row header">
							<div class="cell">
								Pi
							</div>
							<div class="cell">
								IP
							</div>
							<div class="cell">
								Status
							</div>
							<div class="cell">
								Actions
							</div>
						</div>





						<div class="row">
							<div class="cell" data-title="Name">
								Controller
							</div>
							<div class="cell" data-title="ip">
							<?php
							echo shell_exec("/sbin/ip addr show | /bin/grep inet | /bin/grep -v 'host lo' | /bin/grep -v inet6 | /usr/bin/awk '{print $2}'");
							?>
							</div>
							<div class="cell" data-title="Status">
								<font color=green>Online</font>
							</div>
							<div class="cell" data-title="Actions">
								 <a href="#" onclick="reboot()">Reboot </a>
							</div>
						</div>

						<div class="row">
							<div class="cell" data-title="Name">
								p1
							</div>
							<div class="cell" data-title="ip">
							<?php
								echo shell_exec("grep p1 /etc/hosts | awk '{print $1}'");
							?>
							</div>
                                                        </script>
							<div class="cell" data-title="Status" id="p1">
							</div>
							<div class="cell" data-title="Actions">
								<a href="#" onclick="return stopp1();"><i class="fas fa-stop"></i>&nbsp;</a> <a href="#" onclick="return startp1();">&nbsp;<i class="fas fa-play">&nbsp;</i> </a> <a href="api.php?pi=p1&action=vnc" target="_blank">&nbsp;<i class="fas fa-terminal"></i> </a>
							</div>
						</div>

						<div class="row">
							<div class="cell" data-title="Name">
								p2
							</div>
							<div class="cell" data-title="IP">
							    <?php
                                                                echo shell_exec("grep p2 /etc/hosts | awk '{print $1}'");
                                                        ?>
							</div>

							<div class="cell" data-title="Status" id="p2">
							</div>
							<div class="cell" data-title="Actions">
                                                                <a href="#" onclick="return stopp2();"> <i class="fas fa-stop"></i>&nbsp;</a>&nbsp;<a href="#" onclick="return startp2();">&nbsp;<i class="fas fa-play"></i>&nbsp;</a>&nbsp;<a href="api.php?pi=p2&action=vnc" target="_blank">&nbsp;<i class="fas fa-terminal"></i>&nbsp;</a>

							</div>
						</div>

						<div class="row">
							<div class="cell" data-title="Name">
								p3
							</div>
							<div class="cell" data-title="IP">
							<?php
                                                                echo shell_exec("grep p3 /etc/hosts | awk '{print $1}'");
                                                        ?>
							</div>
							<div class="cell" data-title="Status" id="p3">
							</div>
							<div class="cell" data-title="Actions">
							<a href="#" onclick="return stopp3();"> <i class="fas fa-stop"> </i> &nbsp;</a>&nbsp;<a href="#" onclick="return startp3();"><i class="fas fa-play"></i>&nbsp;</a>&nbsp; <a href="api.php?pi=p3&action=vnc" target="_blank">  <i class="fas fa-terminal"></i></a>
							</div>
						</div>

						<div class="row">
							<div class="cell" data-title="Name">
								p4
							</div>
							<div class="cell" data-title="IP">
							<?php
                                                                echo shell_exec("grep p4 /etc/hosts | awk '{print $1}'");
                                                        ?>
							</div>

							<div class="cell" data-title="Status" id="p4">
							</div>
							<div class="cell" data-title="Actions">
                                                        <a href="#" onclick="return stopp4();"> <i class="fas fa-stop"></i> </a>&nbsp;&nbsp;<a href="#" onclick="return startp4();"><i class="fas fa-play"></i> </a>&nbsp;<a href="api.php?pi=p4&action=vnc" target="_blank">  <i class="fas fa-terminal"></i> </a>
							</div>
						</div>

			</div>
		</div>
	</div>



<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>
