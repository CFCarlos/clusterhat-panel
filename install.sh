#!/bin/bash
echo "
   ________           __            __  __      __
  / ____/ /_  _______/ /____  _____/ / / /___ _/ /_
 / /   / / / / / ___/ __/ _ \/ ___/ /_/ / __  / __/
/ /___/ / /_/ (__  ) /_/  __/ /  / __  / /_/ / /_
\____/_/\__,_/____/\__/\___/_/  /_/ /_/\__,_/\__/
					Panel
    ____           __        ____
   /  _/___  _____/ /_____ _/ / /__  _____
   / // __ \/ ___/ __/ __  / / / _ \/ ___/
 _/ // / / (__  ) /_/ /_/ / / /  __/ /
/___/_/ /_/____/\__/\__,_/_/_/\___/_/
				V0.1 Alpha
"

echo 'THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.' | fold -s
echo "This is ALPHA software and WILL be buggy/unstable. Use with caution."
echo "Support can be provided at https://blog.jmdawson.co.uk"
echo
echo
yes | sudo apt install php

echo -n -e  "\e[93mEnter the IP of the first pi (p1)\e[0m"
echo
read p1
echo -n -e "\e[93mEnter the IP of the second pi (p2)\e[0m"
echo
read p2
echo -n -e "\e[93mEnter the IP of the third pi (p3)\e[0m"
echo
read p3
echo -n -e "\e[93mEnter the IP of the fourth pi (p4)\e[0m"
echo
read p4
pis="$p1 $p2 $p3 $p4"
if
cat /etc/hosts | grep -e $p1 -e $p2 -e $p3 -e $p4 >> /dev/null
then
echo -e "\e[91m [ERROR] Installer has already been ran or /etc/hosts already contains the pi's remove these from /etc/hosts and try again\e[0m"
exit
else
echo "controller	127.0.0.1" | sudo tee -a /etc/hosts
echo "$p1       p1" | sudo tee -a /etc/hosts
echo "$p2       p2" | sudo tee -a /etc/hosts
echo "$p3       p3" | sudo tee -a /etc/hosts
echo "$p4       p4" | sudo tee -a /etc/hosts
fi

if [ -f ~/.ssh/id_rsa ]
then
echo "ssh key already generated, continuing"
else
echo -e "\e[93m [REQUIRED] Generating SSH keys, follow the instructions below leaving all options default. Do not inclued a passphrase\e[0m"
ssh-keygen
fi
keyfile=`cat ~/.ssh/id_rsa.pub`

echo "Copying the keyfile to each pi in the cluster"

echo -e  "\e[95m###########################
# Installing to Pi Zeros  #
###########################\e[0m"


for ips in $pis
do

echo -e "\e[34m [INFO] Installing to $ips\e[0m"
echo -e "\e[93m [REQUIRED] Please type the password for $ips if prompted below.\e[0m"
echo -e "\e[34m [INFO] Adding SSH Keys and configuring X for VNC.\e[0m"
ssh pi@$ips "mkdir -p .ssh && echo $keyfile >> .ssh/authorized_keys && echo 'xsetroot -solid gray
xmodmap -e "keysym Super_L = Multi_key"
xset s off; xset dpms 0 1800 0
exec xterm -maximized' >> .xsessionrc && yes | sudo apt install xterm tightvncserver 2>&1"  >> /dev/null
echo -e "\033[32m Done\e[0m"
echo -e "\e[34m [INFO] Adding service for VNC\e[0m"
scp vnc.service pi@$ips:/home/pi/
echo -e "\033[32m Done\e[0m"
echo -e "\e[34m [INFO] Enabling VNC service\e[0m"
ssh pi@$ips "sudo mv /home/pi/vnc.service /etc/systemd/system/vnc.service" 2>&1 >> /dev/null
ssh -t -t -o LogLevel=QUIET pi@$ips 'sudo su - root -c "systemctl enable vnc.service"' 2>&1 >> /dev/null
echo -e "\033[32m Done\e[0m"
echo -e "\e[34m [INFO] Starting VNC Service\e[0m"
ssh -t -t -o LogLevel=QUIET pi@$ips 'sudo su - root -c "systemctl start vnc.service"' 2>&1 >> /dev/null
echo -e "\033[32m Done\e[0m"
echo -e "\e[34m [INFO] Install to $ips complete\e[0m"

done
bash run.sh >> /dev/null
ip=`/sbin/ip addr show | /bin/grep inet | /bin/grep -v 'host lo' | /bin/grep -v inet6 | /usr/bin/awk '{print $2}' | awk -F '/' '{print $1'}`
echo "Clusterhat Panel can now be accessed at http://$ip:8002"
