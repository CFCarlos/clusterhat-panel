#/bin/bash
#Start VNC Services
#p1
nohup bash noVNC/utils/launch.sh --vnc p1:5901 6081 >> /dev/null 2>&1 &
#p2
nohup bash noVNC/utils/launch.sh --vnc p2:5901 6082 >> /dev/null 2>&1 &
#p3
nohup bash noVNC/utils/launch.sh --vnc p3:5901 6083 >> /dev/null 2>&1 &
#p4
nohup bash noVNC/utils/launch.sh --vnc p4:5901 6084 >> /dev/null 2>&1 &



nohup php -S 0.0.0.0:8002 >> /dev/null 2>&1 &
ip=`/sbin/ip addr show | /bin/grep inet | /bin/grep -v 'host lo' | /bin/grep -v inet6 | /usr/bin/awk '{print $2}' | awk -F '/' '{print $1'}`
echo "Clusterhat Panel can now be accessed at http://$ip:8002"
